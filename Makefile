all:
	pdflatex -halt-on-error cv.tex
clean:
	rm *.aux *.log *.out *.pdf

.PHONY: all clean
